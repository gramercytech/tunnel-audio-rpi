# Tunnel-Audio Raspberry Pi

This app auto-starts on boot-up and plays an audio file when raspberry pi receives a keyboard press (any). Subsequent keyboard press (any) fades the audio out.

# Setup:

Step 1 - Clone repo to /home/pi/  

Step 2 - Copy audio file to /home/pi/tunnel-audio-rpi/  

Step 3 - Install pygame  
```sudo apt-get install python-pygame -y```  

Step 4 - Install Inputs  
```sudo pip install inputs```  

Step 5 - Set output volume to maximum  
```amixer sset PCM,0 100%```  

Step 6 - Test output volume  
```speaker-test -t sine -f 600 > /dev/null```  

Step 7 - Enable auto-start on boot  
Edit:  
```/etc/rc.local```  

Add to beginning of file  
```sleep 10```  
```sudo python /home/pi/tunnel-audio-rpi/play.py```  

Step 8 - Plug in the speaker and pressure mat  

Step 9 - Reboot  