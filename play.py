from inputs import get_key
import pygame
import time

pygame.mixer.init()
pygame.mixer.music.set_volume(1.0)

while 1:
	events = get_key()
	for event in events:
		#print(event.ev_type, event.code, event.state)
		if event.ev_type == "Key":
			# print(event.ev_type, event.code, event.state)
			if event.state == 1:
				if pygame.mixer.music.get_busy():
					print "Fading out"
					# pygame.mixer.music.fadeout(2000)
				else:
					print "Playing"
					# pygame.mixer.music.load("/home/pi/tunnel-audio-rpi/xolair_quotescape_v04.mp3")
					pygame.mixer.music.load("/home/pi/tunnel-audio-rpi/trimmed_xolair.mp3")
					pygame.mixer.music.play()